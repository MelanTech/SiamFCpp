import math
import os
import random
import time
from collections import OrderedDict
from datetime import datetime

import torch.cuda
from torch.optim import SGD

from got10k.datasets import *
from torch.utils.data import DataLoader
from tqdm import tqdm
from tensorboardX import SummaryWriter

from siamfcpp.dataset.dataset import GTDataset, CustomDataset
from siamfcpp.dataset.filters import TrackPairFilter
from siamfcpp.dataset.sampler import Sampler
from siamfcpp.dataset.target import DenseboxTarget
from siamfcpp.dataset.transfomer import RandomCropTransformer
from siamfcpp.siamfcpp import TrackerSiamFCPP
from siamfcpp.model.backbone import AlexNet
from siamfcpp.model.head import DenseboxHead
from siamfcpp.loss import IOULoss, SigmoidCrossEntropyCenterness, SigmoidCrossEntropyRetina
import siamfcpp.cfg as cfg
from siamfcpp.utils import move_data_to_device

backbone = AlexNet()
head = DenseboxHead()
model_loss = {
    "IOULoss": IOULoss(),
    "SCER": SigmoidCrossEntropyRetina(),
    "SCEC": SigmoidCrossEntropyCenterness(),
}

model = TrackerSiamFCPP(backbone, head, model_loss)

# 设置设备
if torch.cuda.is_available():
    model.set_device('cuda:0')
else:
    model.set_device('cpu')

optimizer = SGD(model.parameters(),
                lr=cfg.optim.lr,
                momentum=cfg.optim.momentum,
                weight_decay=cfg.optim.weight_decay)


def cosine_scheduler_warmup(optim, current_epoch, max_epoch, lr_min=0., lr_max=0.1, warmup=True):
    warmup_epoch = 10 if warmup else 0
    if current_epoch < warmup_epoch:
        lr = lr_max * current_epoch / warmup_epoch
    else:
        lr = lr_min + (lr_max - lr_min) * \
             (1 + math.cos(math.pi * (current_epoch - warmup_epoch) / (max_epoch - warmup_epoch))) / 2
    for param_group in optim.param_groups:
        param_group['lr'] = lr


# 加载数据集
dataset_dir = r'F:/Project/VOT/Dataset/OTB100'
seqs = OTB(dataset_dir, version=2015, download=False)

gd_dataset = GTDataset(seqs)
tp_filter = TrackPairFilter()
sampler = Sampler(gd_dataset, data_filter=tp_filter, seed=random.randint(0, 1000))

transforms = [
    RandomCropTransformer(),
    DenseboxTarget(),
]

dataset = CustomDataset(sampler,
                        transforms,
                        num_epochs=cfg.train.epochs,
                        nr_image_per_epoch=cfg.train.nr_image_per_epoch)

dataloader = DataLoader(dataset,
                        batch_size=cfg.train.batch_size,
                        shuffle=False,
                        num_workers=cfg.train.num_workers,
                        pin_memory=cfg.train.pin_memory,
                        drop_last=True)

# 训练初始化
torch.cuda.empty_cache()
model.train()

# 加载预训练权重
if cfg.train.pretrain_model_path != '':
    weights = torch.load(cfg.train.pretrain_model_path, map_location=torch.device('cuda:0'))
    model.load_state_dict(weights['model'], strict=True)


def save_model(epochs, model_dict, optim_dict, path):
    ckpt = dict(
        epoch=epochs,
        model=model_dict,
        optim=optim_dict
    )

    torch.save(ckpt, path)


def write_log(writer, reg, cls, ctr, total, lr, iou):
    writer.add_scalar('Loss/reg', reg, global_step=iters)
    writer.add_scalar('Loss/cls', cls, global_step=iters)
    writer.add_scalar('Loss/ctr', ctr, global_step=iters)
    writer.add_scalar('Loss/total', total, global_step=iters)
    writer.add_scalar('Other/lr', lr, global_step=iters)
    writer.add_scalar('Other/iou', iou, global_step=iters)


if __name__ == '__main__':
    timestamp = int(time.time())
    data_iter = iter(dataloader)
    total_loss = None

    iters = 0
    logger = SummaryWriter(logdir='logs/{}_{}'.format(datetime.now().strftime("%Y%m%d"),
                                                      timestamp),
                           flush_secs=5)

    # 开始训练
    for epoch in range(cfg.train.epochs):
        pbar = tqdm(range(cfg.train.max_iter_num))

        for iteration in pbar:

            try:
                train_data = next(data_iter)
            except StopIteration:
                data_iter = iter(dataloader)
                train_data = next(data_iter)

            if torch.cuda.is_available():
                train_data = move_data_to_device(train_data, 'cuda:0')

            cosine_scheduler_warmup(optimizer, epoch + 1, cfg.train.epochs,
                                    lr_min=1e-6, lr_max=0.08, warmup=True)

            optimizer.zero_grad()
            pred_data = model(train_data)
            losses, extras = OrderedDict(), OrderedDict()
            for loss_name, loss in model_loss.items():
                losses[loss_name], extras[loss_name] = loss(pred_data, train_data)

            total_loss = sum(losses.values())
            total_loss.backward()
            optimizer.step()

            pbar.set_description("Epoch {}".format(epoch + 1))
            pbar.set_postfix(lr=optimizer.param_groups[0]['lr'],
                             reg=losses['IOULoss'].item(),
                             cls=losses['SCER'].item(),
                             ctr=losses['SCEC'].item(),
                             iou=extras['IOULoss']['iou'].item())

            write_log(logger,
                      reg=losses['IOULoss'].item(),
                      cls=losses['SCER'].item(),
                      ctr=losses['SCEC'].item(),
                      total=total_loss,
                      lr=optimizer.param_groups[0]['lr'],
                      iou=extras['IOULoss']['iou'].item())

            iters += 1

        # 保存模型
        if not os.path.exists(cfg.train.save_dir):
            os.makedirs(cfg.train.save_dir)

        if (epoch + 1) % cfg.train.save_per_epoch == 0:
            save_model(epoch,
                       model.state_dict(),
                       optimizer.state_dict(),
                       os.path.join(cfg.train.save_dir,
                                    "{}_e{}_loss_{}.pth".format(timestamp, epoch + 1,
                                                                total_loss.item())))

    save_model(cfg.train.epochs,
               model.state_dict(),
               optimizer.state_dict(),
               os.path.join(cfg.train.save_dir,
                            "{}_e{}_loss_{}.pth".format(timestamp, cfg.train.epochs,
                                                        total_loss.item())))

    logger.close()
