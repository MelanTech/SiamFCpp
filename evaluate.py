from __future__ import absolute_import

import os

import torch
from got10k.experiments import *
from siamfcpp.model.backbone import AlexNet
from siamfcpp.model.head import DenseboxHead
from siamfcpp.siamfcpp import TrackerSiamFCPP
from siamfcpp.tracker.tracker import TrackerSiamFCpp

backbone = AlexNet()
head = DenseboxHead()

model = TrackerSiamFCPP(backbone, head)

# 设置设备
if torch.cuda.is_available():
    model.set_device('cuda:0')
else:
    model.set_device('cpu')


weight_path = 'models/checkpoints/1679401572_e38_loss_0.9928303956985474.pth'

weights = torch.load(weight_path, map_location=torch.device('cuda:0'))
model.load_state_dict(weights['model'], strict=True)

model.eval()
tracker = TrackerSiamFCpp(model, torch.device('cuda:0'))
tracker.__setattr__('name', 'SiamFC++')
tracker.__setattr__('is_deterministic', True)

if __name__ == '__main__':
    root_dir = os.path.expanduser('../Dataset/OTB100')
    e = ExperimentOTB(root_dir, version=2015)
    e.run(tracker)
    e.report([tracker.name])
