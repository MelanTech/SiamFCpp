from time import time

import cv2
import torch

from siamfcpp import cfg
from siamfcpp.model.backbone import AlexNet
from siamfcpp.model.head import DenseboxHead
from siamfcpp.siamfcpp import TrackerSiamFCPP
from siamfcpp.tracker.tracker import TrackerSiamFCpp

backbone = AlexNet()
head = DenseboxHead()

model = TrackerSiamFCPP(backbone, head)

# 设置设备
if torch.cuda.is_available():
    model.set_device('cuda:0')
else:
    model.set_device('cpu')


weight_path = 'models/checkpoints/1679401572_e38_loss_0.9928303956985474.pth'
torch.load(weight_path)
weights = torch.load(weight_path, map_location=torch.device('cuda:0'))
model.load_state_dict(weights['model'], strict=True)

model.eval()
tracker = TrackerSiamFCpp(model, torch.device('cuda:0'))

# 变量
selectingObject = False
initTracking = False
onTracking = False

ix, iy, cx, cy = -1, -1, -1, -1
w, h = 0, 0

interval = 15
duration = 0.01


def draw_bbox(event, x, y, flags, params):
    global selectingObject, initTracking, onTracking, ix, iy, cx, cy, w, h

    if event == cv2.EVENT_LBUTTONDOWN:
        selectingObject = True
        onTracking = False
        ix, iy = x, y
        cx, cy = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        cx, cy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        selectingObject = False
        if abs(x - ix) > 10 and abs(y - iy) > 10:
            w, h = abs(x - ix), abs(y - iy)
            ix, iy = min(x, ix), min(y, iy)
            initTracking = True
        else:
            onTracking = False

    elif event == cv2.EVENT_RBUTTONDOWN:
        onTracking = False
        if w > 0:
            ix, iy = x - w / 2, y - h / 2
            initTracking = True


if __name__ == '__main__':
    cap = cv2.VideoCapture(r'F:\Project\VOT\KCF\videos\test.avi')

    cv2.namedWindow('tracking')
    cv2.setMouseCallback('tracking', draw_bbox)

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        if selectingObject:
            cv2.rectangle(frame, (ix, iy), (cx, cy), (0, 255, 255), 1)
        elif initTracking:
            cv2.rectangle(frame, (ix, iy), (ix + w, iy + h), (0, 255, 255), 2)
            tracker.init(frame, [ix, iy, w, h])

            initTracking = False
            onTracking = True
        elif onTracking:
            t0 = time()
            bbox = tracker.update(frame)
            t1 = time()

            bbox = list(map(int, bbox))
            cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[0] + bbox[2], bbox[1] + bbox[3]), (0, 255, 0), 2)

            duration = t1 - t0

            cv2.putText(frame, 'FPS: ' + str(1 / duration)[:4].strip('.'), (8, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6,
                        (0, 0, 255), 2)

        cv2.imshow('tracking', frame)
        c = cv2.waitKey(interval) & 0xFF
        if c == 27 or c == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
