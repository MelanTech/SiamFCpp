import numpy as np
import torch
import torch.nn as nn

import siamfcpp.cfg as cfg

eps = np.finfo(np.float32).tiny


class SafeLog(nn.Module):
    """
    安全执行Log运算
    """

    def __init__(self):
        super(SafeLog, self).__init__()
        self.register_buffer("t_eps", torch.tensor(eps, requires_grad=False))

    def forward(self, t):
        return torch.log(torch.max(self.t_eps, t))


class IOULoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.safelog = SafeLog()
        self.register_buffer("t_one", torch.tensor(1., requires_grad=False))
        self.register_buffer("t_zero", torch.tensor(0., requires_grad=False))

        self.background = cfg.loss.IOU.background
        self.ignore_label = cfg.loss.IOU.ignore_label
        self.weight = cfg.loss.IOU.weight

    def forward(self, pred_data, target_data):
        pred = pred_data["box_pred"]
        gt = target_data["box_gt"]
        cls_gt = target_data["cls_gt"]

        # 创建一个掩码，用于在计算 IoU 损失时忽略一些特定的标签
        mask = ((~(cls_gt == self.background)) *
                (~(cls_gt == self.ignore_label))).detach()
        mask = mask.type(torch.Tensor).squeeze(2).to(pred.device)

        # 计算目标框的面积，其中 aog 表示实际目标框的面积，aop 表示模型预测输出的目标框的面积
        aog = torch.abs(gt[:, :, 2] - gt[:, :, 0] + 1) * torch.abs(gt[:, :, 3] - gt[:, :, 1] + 1)
        aop = torch.abs(pred[:, :, 2] - pred[:, :, 0] + 1) * torch.abs(pred[:, :, 3] - pred[:, :, 1] + 1)

        # 计算交集区域的面积
        # iw 和 ih 分别表示预测框和实际框在水平和垂直方向上的重叠部分长度
        # inter 表示它们的交集面积
        iw = torch.min(pred[:, :, 2], gt[:, :, 2]) - torch.max(
            pred[:, :, 0], gt[:, :, 0]) + 1
        ih = torch.min(pred[:, :, 3], gt[:, :, 3]) - torch.max(
            pred[:, :, 1], gt[:, :, 1]) + 1
        inter = torch.max(iw, self.t_zero) * torch.max(ih, self.t_zero)

        union = aog + aop - inter
        iou = torch.max(inter / union, self.t_zero)

        # 计算平均损失并进行加权，加权过程基于掩码将损失值限制在仅考虑有用信息的区域内
        loss = -self.safelog(iou)
        loss = (loss * mask).sum() / torch.max(mask.sum(), self.t_one) * self.weight

        # 计算平均IOU
        iou = iou.detach()
        iou = (iou * mask).sum() / torch.max(mask.sum(), self.t_one)
        extra = dict(iou=iou)

        return loss, extra


class SigmoidCrossEntropyRetina(nn.Module):
    def __init__(self):
        super(SigmoidCrossEntropyRetina, self).__init__()
        self.safelog = SafeLog()
        self.register_buffer("t_one", torch.tensor(1., requires_grad=False))

        self.background = cfg.loss.SCER.background
        self.ignore_label = cfg.loss.SCER.ignore_label
        self.weight = cfg.loss.SCER.weight

        # focal loss coefficients
        self.register_buffer("alpha", torch.tensor(float(cfg.loss.SCER.alpha), requires_grad=False))
        self.register_buffer("gamma", torch.tensor(float(cfg.loss.SCER.gamma), requires_grad=False))

    def forward(self, pred_data, target_data):
        r"""
        Focal loss
        :param pred_data: shape=(B, HW, C), classification logits (BEFORE Sigmoid)
        :param target_data: shape=(B, HW)
        """
        pred = pred_data["cls_pred"]
        label = target_data["cls_gt"]

        mask = ~(label == self.ignore_label)
        mask = mask.type(torch.Tensor).to(label.device)

        # 将目标值和掩码张量相乘，生成一个处理过的目标值张量（vlabel）
        # 可以理解为将原始目标值中被忽略的位置清零
        vlabel = label * mask

        zero_mat = torch.zeros(pred.shape[0], pred.shape[1], pred.shape[2] + 1)
        one_mat = torch.ones(pred.shape[0], pred.shape[1], pred.shape[2] + 1)
        index_mat = vlabel.type(torch.LongTensor)

        onehot_ = zero_mat.scatter(2, index_mat, one_mat)
        onehot = onehot_[:, :, 1:].type(torch.Tensor).to(pred.device)

        # 计算Focal Loss
        pred = torch.sigmoid(pred)
        pos_part = (1 - pred) ** self.gamma * onehot * self.safelog(pred)
        neg_part = pred ** self.gamma * (1 - onehot) * self.safelog(1 - pred)
        loss = -(self.alpha * pos_part + (1 - self.alpha) * neg_part).sum(dim=2) * mask.squeeze(2)

        positive_mask = (label > 0).type(torch.Tensor).to(pred.device)

        loss = loss.sum() / torch.max(positive_mask.sum(),
                                      self.t_one) * self.weight
        extra = dict()

        return loss, extra


class SigmoidCrossEntropyCenterness(nn.Module):
    def __init__(self):
        super(SigmoidCrossEntropyCenterness, self).__init__()
        self.safelog = SafeLog()
        self.register_buffer("t_one", torch.tensor(1., requires_grad=False))
        self.background = cfg.loss.SCEC.background
        self.ignore_label = cfg.loss.SCEC.ignore_label
        self.weight = cfg.loss.SCEC.weight

    def forward(self, pred_data, target_data):
        r"""
        Center-ness loss
        pred: torch.Tensor
            center-ness logits (BEFORE Sigmoid)
            format: (B, HW)
        label: torch.Tensor
            training label
            format: (B, HW)

        Returns
        -------
        torch.Tensor
            scalar loss
            format: (,)
        """
        pred = pred_data["ctr_pred"]
        label = target_data["ctr_gt"]

        mask = (~(label == self.background)).type(torch.Tensor).to(pred.device)
        not_neg_mask = (pred >= 0).type(torch.Tensor).to(pred.device)

        loss = (pred * not_neg_mask - pred * label + self.safelog(1. + torch.exp(-torch.abs(pred)))) * mask
        loss_residual = (-label * self.safelog(label) - (1 - label) * self.safelog(
            1 - label)) * mask  # suppress loss residual (original vers.)
        loss = loss - loss_residual.detach()
        loss = loss.sum() / torch.max(mask.sum(), self.t_one) * self.weight
        extra = dict()

        return loss, extra
