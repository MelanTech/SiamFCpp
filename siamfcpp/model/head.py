import numpy as np

import torch
import torch.nn as nn

import siamfcpp.cfg as cfg
from siamfcpp.model.blocks import conv_bn_relu


def get_xy_ctr(score_size, score_offset, total_stride):
    """
    计算特征图上每个像素点的中心坐标
    :param score_size: 特征图大小
    :param score_offset:  特征图偏置，确保这些点是以特征图中心计算的
    :param total_stride:  总下采样步长
    :return:  所有的中心坐标
    """
    batch, fm_height, fm_width = 1, score_size, score_size

    # .broadcast([1, fm_height, fm_width, 1])
    y_list = torch.linspace(0., fm_height - 1., fm_height) \
        .reshape(1, fm_height, 1, 1) \
        .repeat(1, 1, fm_width, 1)

    # .broadcast([1, fm_height, fm_width, 1])
    x_list = torch.linspace(0., fm_width - 1., fm_width) \
        .reshape(1, 1, fm_width, 1) \
        .repeat(1, fm_height, 1, 1)

    xy_list = score_offset + torch.cat([x_list, y_list], 3) * total_stride

    # .broadcast([batch, fm_height, fm_width, 2]).reshape(batch, -1, 2)
    xy_ctr = xy_list \
        .repeat(batch, 1, 1, 1) \
        .reshape(batch, -1, 2)

    xy_ctr = xy_ctr.type(torch.Tensor)
    return xy_ctr


def get_box(xy_ctr, offsets):
    """
    预测边界框
    :param xy_ctr:  中心点坐标
    :param offsets:  偏移量
    :return:  边界框
    """
    offsets = offsets.permute(0, 2, 3, 1)  # (B, H, W, C), C=4
    offsets = offsets.reshape(offsets.shape[0], -1, 4)

    # 计算左上角以及右下角坐标
    xy0 = (xy_ctr[:, :, :] - offsets[:, :, :2])
    xy1 = (xy_ctr[:, :, :] + offsets[:, :, 2:])
    # 拼接两个坐标，得到对应的边界框
    bboxes_pred = torch.cat([xy0, xy1], 2)

    return bboxes_pred


class DenseboxHead(nn.Module):
    """
    Densebox Head
    """

    def __init__(self):
        super(DenseboxHead, self).__init__()

        self.bi = torch.nn.Parameter(torch.tensor(0.).type(dtype=torch.Tensor))
        self.si = torch.nn.Parameter(torch.tensor(1.).type(dtype=torch.Tensor))

        self.cls_convs = []
        self.bbox_convs = []

        self.score_size = cfg.head.score_size
        self.total_stride = cfg.head.total_stride

        # 基于输入大小、得分大小和步长计算得分图的偏移量。使用整数除法运算符 (//) 确保结果是一个整数。
        self.score_offset = (cfg.head.x_size - 1 - (self.score_size - 1) * self.total_stride) // 2

        ctr = get_xy_ctr(self.score_size, self.score_offset, self.total_stride)
        self.fm_ctr = ctr
        self.fm_ctr.require_grad = False

        self._make_conv3x3()
        self._make_conv_output()
        self._initialize_conv()

    def forward(self, c_out, r_out):
        # 分类头
        num_conv3x3 = cfg.head.num_conv3x3
        cls = c_out
        bbox = r_out

        for i in range(0, num_conv3x3):
            cls = getattr(self, 'cls_p5_conv%d' % (i + 1))(cls)
            bbox = getattr(self, 'bbox_p5_conv%d' % (i + 1))(bbox)

        # 分类得分
        cls_score = self.cls_score_p5(cls)
        cls_score = cls_score.permute(0, 2, 3, 1)
        cls_score = cls_score.reshape(cls_score.shape[0], -1, 1)

        # 中心度得分
        ctr_score = self.ctr_score_p5(cls)
        ctr_score = ctr_score.permute(0, 2, 3, 1)
        ctr_score = ctr_score.reshape(ctr_score.shape[0], -1, 1)

        # 回归
        offsets = self.bbox_offsets_p5(bbox)
        offsets = torch.exp(self.si * offsets + self.bi) * self.total_stride

        # bbox解码
        self.fm_ctr = self.fm_ctr.to(offsets.device)
        bbox = get_box(self.fm_ctr, offsets)

        return [cls_score, ctr_score, bbox, cls]

    def _make_conv3x3(self):
        num_conv3x3 = cfg.head.num_conv3x3
        head_conv_bn = cfg.head.head_conv_bn
        head_width = cfg.head.head_width

        self.cls_conv3x3_list = []
        self.bbox_conv3x3_list = []

        for i in range(num_conv3x3):
            cls_conv3x3 = conv_bn_relu(
                head_width,
                head_width,
                stride=1,
                ksize=3,
                pad=0,
                has_bn=head_conv_bn[i]
            )

            bbox_conv3x3 = conv_bn_relu(
                head_width,
                head_width,
                stride=1,
                ksize=3,
                pad=0,
                has_bn=head_conv_bn[i]
            )

            setattr(self, 'cls_p5_conv%d' % (i + 1), cls_conv3x3)
            setattr(self, 'bbox_p5_conv%d' % (i + 1), bbox_conv3x3)

            self.cls_conv3x3_list.append(cls_conv3x3)
            self.bbox_conv3x3_list.append(bbox_conv3x3)

    def _make_conv_output(self):
        head_width = cfg.head.head_width
        self.cls_score_p5 = conv_bn_relu(
            head_width,
            1,
            stride=1,
            ksize=1,
            pad=0,
            has_relu=False
        )
        self.ctr_score_p5 = conv_bn_relu(
            head_width,
            1,
            stride=1,
            ksize=1,
            pad=0,
            has_relu=False
        )
        self.bbox_offsets_p5 = conv_bn_relu(
            head_width,
            4,
            stride=1,
            ksize=1,
            pad=0,
            has_relu=False
        )

    def _initialize_conv(self):
        """
        卷积层初始化权重
        :return:
        """
        num_conv3x3 = cfg.head.num_conv3x3
        conv_weight_std = cfg.head.conv_weight_std

        # 初始化头
        conv_list = []
        for i in range(num_conv3x3):
            conv_list.append(getattr(self, 'cls_p5_conv%d' % (i + 1)).conv)
            conv_list.append(getattr(self, 'bbox_p5_conv%d' % (i + 1)).conv)

        conv_list.append(self.cls_score_p5.conv)
        conv_list.append(self.ctr_score_p5.conv)
        conv_list.append(self.bbox_offsets_p5.conv)
        conv_classifier = [self.cls_score_p5.conv]
        assert all(elem in conv_list for elem in conv_classifier)

        pi = 0.01
        bv = -np.log((1 - pi) / pi)
        for ith in range(len(conv_list)):
            # 从列表中提取conv层
            conv = conv_list[ith]
            torch.nn.init.normal_(conv.weight, std=conv_weight_std)  # conv_weight_std = 0.0001
            if conv in conv_classifier:
                # 如果是Classifier则对分类器层的偏置进行常数初始化，值为 bv
                torch.nn.init.constant_(conv.bias, torch.tensor(bv))
            else:
                fan_in, _ = nn.init._calculate_fan_in_and_fan_out(conv.weight)
                bound = 1 / np.sqrt(fan_in)
                # 对当前卷积层的偏置进行均匀分布初始化，值为一个以 0 为中心，以 bound 为半径的均匀分布。
                nn.init.uniform_(conv.bias, -bound, bound)

