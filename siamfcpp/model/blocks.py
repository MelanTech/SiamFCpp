from torch import nn
import torch.nn.functional as F


class conv_bn_relu(nn.Module):
    """
    可选BN与Relu的卷积层
    """

    def __init__(self,
                 in_channel,
                 out_channel,
                 stride=1,
                 ksize=3,
                 pad=0,
                 has_bn=True,
                 has_relu=True,
                 bias=True,
                 groups=1):
        super(conv_bn_relu, self).__init__()
        self.conv = nn.Conv2d(in_channel,
                              out_channel,
                              kernel_size=ksize,
                              stride=stride,
                              padding=pad,
                              bias=bias,
                              groups=groups)

        if has_bn:
            self.bn = nn.BatchNorm2d(out_channel)
        else:
            self.bn = None

        if has_relu:
            self.relu = nn.ReLU()
        else:
            self.relu = None

    def forward(self, x):
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        return x


def xcorr_depthwise(x, kernel):
    """
    深度互相关，例如用于Siam跟踪网络中的模板匹配

    参数
    ---------
    x: torch.Tensor feature_x (例如SOT中的搜索区域特征)
    kernel: torch.Tensor feature_z (例如SOT中的模板特征)

    Returns
    -------
    torch.Tensor 互相关结果
    """
    batch = kernel.size(0)
    channel = kernel.size(1)
    x = x.view(1, batch * channel, x.size(2), x.size(3))
    kernel = kernel.view(batch * channel, 1, kernel.size(2), kernel.size(3))
    out = F.conv2d(x, kernel, groups=batch * channel)
    out = out.view(batch, channel, out.size(2), out.size(3))

    return out
