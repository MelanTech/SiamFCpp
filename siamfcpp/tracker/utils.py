import cv2
import numpy as np
import torch

from siamfcpp.utils import cxywh2xyxy


def get_subwindow_tracking(im,
                           pos,
                           model_sz,
                           original_sz,
                           avg_chans=(0, 0, 0),
                           mask=None):
    crop_cxywh = np.concatenate([np.array(pos), np.array((original_sz, original_sz))], axis=-1)
    crop_xyxy = cxywh2xyxy(crop_cxywh)
    # warpAffine transform matrix
    M_13 = crop_xyxy[0]
    M_23 = crop_xyxy[1]
    M_11 = (crop_xyxy[2] - M_13) / (model_sz - 1)
    M_22 = (crop_xyxy[3] - M_23) / (model_sz - 1)
    mat2x3 = np.array([
        M_11,
        0,
        M_13,
        0,
        M_22,
        M_23,
    ]).reshape(2, 3)
    im_patch = cv2.warpAffine(im,
                              mat2x3, (model_sz, model_sz),
                              flags=(cv2.INTER_LINEAR | cv2.WARP_INVERSE_MAP),
                              borderMode=cv2.BORDER_CONSTANT,
                              borderValue=tuple(map(int, avg_chans)))
    if mask is not None:
        mask_patch = cv2.warpAffine(mask,
                                    mat2x3, (model_sz, model_sz),
                                    flags=(cv2.INTER_NEAREST
                                           | cv2.WARP_INVERSE_MAP))
        return im_patch, mask_patch
    return im_patch


def get_crop(im,
             target_pos,
             target_sz,
             z_size,
             x_size=None,
             avg_chans=(0, 0, 0),
             context_amount=0.5,
             func_get_subwindow=get_subwindow_tracking,
             output_size=None,
             mask=None):
    wc = target_sz[0] + context_amount * sum(target_sz)
    hc = target_sz[1] + context_amount * sum(target_sz)
    s_crop = np.sqrt(wc * hc)
    scale = z_size / s_crop

    # im_pad = x_pad / scale
    if x_size is None:
        x_size = z_size
    s_crop = x_size / scale

    if output_size is None:
        output_size = x_size
    if mask is not None:
        im_crop, mask_crop = func_get_subwindow(im,
                                                target_pos,
                                                output_size,
                                                round(s_crop),
                                                avg_chans,
                                                mask=mask)
        return im_crop, mask_crop, scale
    else:
        im_crop = func_get_subwindow(im, target_pos, output_size, round(s_crop),
                                     avg_chans)
        return im_crop, scale


def imarray_to_tensor(arr):
    r"""
    Transpose & convert from numpy.array to torch.Tensor
    :param arr: numpy.array, (H, W, C)
    :return: torch.Tensor, (1, C, H, W)
    """
    arr = np.ascontiguousarray(
        arr.transpose(2, 0, 1)[np.newaxis, ...], np.float32)
    return torch.from_numpy(arr)


def tensor_to_numpy(t):
    r"""
    Perform naive detach / cpu / numpy process.
    :param t: torch.Tensor, (N, C, H, W)
    :return: numpy.array, (N, C, H, W)
    """
    arr = t.detach().cpu().numpy()
    return arr
