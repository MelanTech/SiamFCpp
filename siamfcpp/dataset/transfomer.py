import numpy as np

from siamfcpp import cfg
from siamfcpp.utils import crop_track_pair


class RandomCropTransformer:
    def __init__(self, seed: int = 0) -> None:
        self.rng = np.random.RandomState(seed)

    def __call__(self, sampled_data):
        data1 = sampled_data["data1"]
        data2 = sampled_data["data2"]
        im_temp, bbox_temp = data1["image"], data1["anno"]
        im_curr, bbox_curr = data2["image"], data2["anno"]
        im_z, bbox_z, im_x, bbox_x, _, _ = crop_track_pair(
            im_temp,
            bbox_temp,
            im_curr,
            bbox_curr,
            config=cfg.transformer,
            rng=self.rng)

        sampled_data["data1"] = dict(image=im_z, anno=bbox_z)
        sampled_data["data2"] = dict(image=im_x, anno=bbox_x)

        return sampled_data
