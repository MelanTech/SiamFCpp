import numpy as np

from siamfcpp import cfg
from siamfcpp.utils import load_image


class Sampler:
    def __init__(self, dataset, seed: int = 0, data_filter=None):
        if data_filter is None:
            self.data_filter = [lambda x: False]
        else:
            self.data_filter = data_filter

        self.dataset = dataset
        self.rng = np.random.RandomState(seed)

        self.ratio = dataset.ratio
        self.max_diff = dataset.max_diff

    def __getitem__(self, item) -> dict:
        is_negative_pair = (self.rng.rand() < cfg.sampler.negative_pair_ratio)
        data1 = data2 = None

        while self.data_filter(data1) or self.data_filter(data2):
            if is_negative_pair:
                data1 = self._sample_track_frame()
                data2 = self._sample_track_frame()
            else:
                data1, data2 = self._sample_track_pair()

            data1["image"] = load_image(data1["image"])
            data2["image"] = load_image(data2["image"])

        sampled_data = dict(
            data1=data1,
            data2=data2,
            is_negative_pair=is_negative_pair,
        )

        return sampled_data

    def _sample_track_frame(self):
        sequence_data = self._sample_sequence_from_dataset(self.dataset)
        data_frame = self._sample_track_frame_from_sequence(sequence_data)

        return data_frame

    def _sample_track_pair(self):
        sequence_data = self._sample_sequence_from_dataset(self.dataset)
        data1, data2 = self._sample_track_pair_from_sequence(sequence_data, self.max_diff)

        return data1, data2

    def _sample_sequence_from_dataset(self, dataset):
        len_dataset = len(dataset)
        idx = self.rng.choice(len_dataset)

        sequence_data = dataset[idx]

        return sequence_data

    def _sample_track_frame_from_sequence(self, sequence_data):
        rng = self.rng
        len_seq = self._get_len_seq(sequence_data)
        idx = rng.choice(len_seq)
        data_frame = {k: v[idx] for k, v in sequence_data.items()}
        return data_frame

    def _sample_track_pair_from_sequence(self, sequence_data, max_diff):
        """sample a pair of frames within max_diff distance

        Parameters
        ----------
        sequence_data : List
            sequence data: image= , anno=
        max_diff : int
            maximum difference of indexes between two frames in the  pair

        Returns
        -------
        Tuple[Dict, Dict]
            track pair data
            data: image= , anno=
        """
        len_seq = self._get_len_seq(sequence_data)
        idx1, idx2 = self._sample_pair_idx_pair_within_max_diff(len_seq, max_diff)
        data1 = {k: v[idx1] for k, v in sequence_data.items()}
        data2 = {k: v[idx2] for k, v in sequence_data.items()}
        return data1, data2

    def _sample_pair_idx_pair_within_max_diff(self, length, max_diff):
        r"""
        在给定的最大差值范围（L）内获取一对索引
        Arguments
        ---------
        length: int
            length
        max_diff: int
            difference
        """
        rng = self.rng
        idx1 = rng.choice(length)
        idx2_choices = list(range(max(0, idx1 - max_diff), idx1)) + list(range(min(idx1 + 1, length), min(idx1 + max_diff, length)))
        idx2 = rng.choice(idx2_choices)
        return int(idx1), int(idx2)

    @staticmethod
    def _get_len_seq(seq_data):
        return len(seq_data["image"])
