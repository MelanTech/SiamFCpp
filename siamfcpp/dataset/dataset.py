from torch.utils.data import Dataset

from siamfcpp import cfg
from siamfcpp.utils import xywh2xyxy, convert_numpy_to_tensor


class CustomDataset(Dataset):
    def __init__(self, sampler, process, num_epochs, nr_image_per_epoch):
        self.sampler = sampler
        self.process = process
        self.num_epochs = num_epochs
        self.nr_image_per_epoch = nr_image_per_epoch

    def __getitem__(self, item):
        sampled_data = self.sampler[item]

        for proc in self.process:
            sampled_data = proc(sampled_data)
        sampled_data = convert_numpy_to_tensor(sampled_data)
        return sampled_data

    def __len__(self):
        return self.nr_image_per_epoch * self.num_epochs


class GTDataset(Dataset):
    """
    GOT10k Toolkit Dataset Adaptor
    """

    def __init__(self, seqs):
        super(GTDataset, self).__init__()
        self.seqs = seqs
        self.ratio = cfg.dataset.ratio
        self.max_diff = cfg.dataset.max_diff

    def __getitem__(self, item: int):
        img_files, anno = self.seqs[item]

        anno = xywh2xyxy(anno)
        sequence_data = dict(image=img_files, anno=anno)

        return sequence_data

    def __len__(self):
        return len(self.seqs)
