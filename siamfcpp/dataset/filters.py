import cv2

from siamfcpp import cfg
from siamfcpp.utils import xyxy2xywh, filter_unreasonable_training_boxes


class TrackPairFilter:
    """
    Tracking data filter
    """

    def __init__(self):
        super().__init__()

    def __call__(self, data):
        if data is None:
            return True
        im, anno = data["image"], data["anno"]
        if cfg.filters.target_type == "bbox":
            bbox = xyxy2xywh(anno)
        elif cfg.filters.target_type == "mask":
            bbox = cv2.boundingRect(anno)
        else:
            exit()
        filter_flag = filter_unreasonable_training_boxes(im, bbox, cfg.filters)
        return filter_flag
