from easydict import EasyDict as eDict

# Head参数
head = eDict(
    total_stride=8,  # int, Backbone中的步长
    score_size=17,  # int, 最终的特征图尺寸
    x_size=303,  # int, 原图像尺寸
    num_conv3x3=3,  # int, Head平铺的conv3x3数量
    head_conv_bn=[False, False, True],  # 卷积中是否包含BN的bool列表
    head_width=256,  # int, Head结构中特征的width
    conv_weight_std=0.0001,  # float, 卷积参数初始化的标准差
)

# Model参数
model = eDict(
    head_width=256,  # Head结构中特征的width
    conv_weight_std=0.01,  # 卷积参数初始化的标准差
    neck_conv_bias=[True, True, True, True],
    corr_fea_output=False,  # 是否输出互相关特征
)

loss = eDict(
    IOU=eDict(
        background=0,
        ignore_label=-1,
        weight=3.0,
    ),
    SCER=eDict(
        background=0,
        ignore_label=-1,
        weight=1.0,
        alpha=0.25,
        gamma=2.0,
    ),
    SCEC=eDict(
        background=0,
        ignore_label=-1,
        weight=1.0,
    )
)

# 图像增强参数
transformer = eDict(
    context_amount=0.5,  # 模板图像的上下文系数
    max_scale=0.3,  # 搜索图像的最大尺度变化率
    max_shift=0.4,  # 搜索图像的最大偏移变化率
    max_scale_temp=0.0,  # 模板图像的最大比例变化率
    max_shift_temp=0.0,  # 模板图像的最大偏移变化率
    z_size=127,  # 模板图像的输出大小
    x_size=303,  # 搜索图像的输出大小
)

# Target参数
target = eDict(
    z_size=127,
    x_size=303,
    score_size=17,
    score_offset=87,
    total_stride=8,
    num_conv3x3=3,
)

# Filter参数
filters = eDict(
    max_area_rate=0.6,
    min_area_rate=0.001,
    max_ratio=10,
    target_type="bbox",
)

# Dataset参数
dataset = eDict(
    dataset_root="datasets/GOT-10k",
    subset="train",
    ratio=1.0,
    max_diff=100,  # 获取帧对最大间隔
    check_integrity=True,
)

# Sampler参数
sampler = eDict(
    negative_pair_ratio=0.0,
    target_type="bbox",
)

# 优化器参数
optim = eDict(
    lr=0.1,
    momentum=0.9,
    weight_decay=0.00005,
    min_lr=1e-8,
)

# 训练参数
train = eDict(
    epochs=1,
    batch_size=32,
    num_workers=2,
    pin_memory=True,
    nr_image_per_epoch=400000,
    max_iter_num=50,
    pretrain_model_path='models/pretrained/model.pth',
    save_per_epoch=10,
    save_dir='models/checkpoints',
)

tracker = eDict(
    total_stride=8,
    score_size=17,
    score_offset=87,
    context_amount=0.5,
    test_lr=0.52,
    penalty_k=0.04,
    window_influence=0.21,
    windowing="cosine",
    z_size=127,
    x_size=303,
    num_conv3x3=3,
    min_w=10,
    min_h=10,
    phase_init="feature",
    phase_track="track",
    corr_fea_output=False,
)
