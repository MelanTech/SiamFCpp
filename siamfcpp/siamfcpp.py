import torch
import torch.nn as nn

from siamfcpp.model.blocks import xcorr_depthwise, conv_bn_relu
import siamfcpp.cfg as cfg


class TrackerSiamFCPP(nn.Module):
    def __init__(self, backbone, head, loss=None):
        super(TrackerSiamFCPP, self).__init__()
        self.backbone = backbone
        self.head = head
        self.loss = loss

        self._make_convs()
        self._initialize_conv()

    def forward(self, *args, phase="train"):
        # 训练阶段
        if phase == 'train':
            # 解析训练数据
            training_data = args[0]
            target_img = training_data["im_z"]
            search_img = training_data["im_x"]

            # 得到Backbone特征
            f_z = self.backbone(target_img)
            f_x = self.backbone(search_img)

            # 特征调整
            c_z_k = self.c_z_k(f_z)
            r_z_k = self.r_z_k(f_z)
            c_x = self.c_x(f_x)
            r_x = self.r_x(f_x)

            # 特征匹配
            r_out = xcorr_depthwise(r_x, r_z_k)
            c_out = xcorr_depthwise(c_x, c_z_k)

            # 送入Head
            fcos_cls_score_final, fcos_ctr_score_final, fcos_bbox_final, corr_fea = self.head(c_out, r_out)
            predict_data = dict(
                cls_pred=fcos_cls_score_final,
                ctr_pred=fcos_ctr_score_final,
                box_pred=fcos_bbox_final,
            )
            if cfg.model.corr_fea_output:
                predict_data["corr_fea"] = corr_fea
            return predict_data

        # 仅输出Backbone特征
        elif phase == 'feature':
            target_img, = args
            # Backbone特征
            f_z = self.backbone(target_img)

            # 使用模板作为卷积核
            c_z_k = self.c_z_k(f_z)
            r_z_k = self.r_z_k(f_z)
            out_list = [c_z_k, r_z_k]

        # 跟踪阶段
        elif phase == 'track':
            if len(args) == 3:
                search_img, c_z_k, r_z_k = args
                # Backbone特征
                f_x = self.backbone(search_img)
                # 特征调整
                c_x = self.c_x(f_x)
                r_x = self.r_x(f_x)
            elif len(args) == 4:
                # c_x, r_x 已经计算得到
                c_z_k, r_z_k, c_x, r_x = args
            else:
                raise ValueError("Illegal args length: %d" % len(args))

            # 特征匹配
            r_out = xcorr_depthwise(r_x, r_z_k)
            c_out = xcorr_depthwise(c_x, c_z_k)

            # 送入Head
            fcos_cls_score_final, fcos_ctr_score_final, fcos_bbox_final, corr_fea = self.head(c_out, r_out)

            # 使用Sigmoid
            fcos_cls_prob_final = torch.sigmoid(fcos_cls_score_final)
            fcos_ctr_prob_final = torch.sigmoid(fcos_ctr_score_final)

            # 进行中心度校正
            fcos_score_final = fcos_cls_prob_final * fcos_ctr_prob_final

            # 保存额外输出
            extra = dict(c_x=c_x, r_x=r_x, corr_fea=corr_fea)

            # 输出
            out_list = fcos_score_final, fcos_bbox_final, fcos_cls_prob_final, fcos_ctr_prob_final, extra
        else:
            raise ValueError("Phase non-implemented.")

        return out_list

    def _make_convs(self):
        head_width = cfg.model.head_width

        # 特征调整
        self.r_z_k = conv_bn_relu(
            head_width,
            head_width,
            stride=1,
            ksize=3,
            pad=0,
            has_relu=False
        )
        self.c_z_k = conv_bn_relu(
            head_width,
            head_width,
            stride=1,
            ksize=3,
            pad=0,
            has_relu=False
        )
        self.r_x = conv_bn_relu(head_width, head_width, 1, 3, 0, has_relu=False)
        self.c_x = conv_bn_relu(head_width, head_width, 1, 3, 0, has_relu=False)

    def _initialize_conv(self, ):
        conv_weight_std = cfg.model.conv_weight_std
        conv_list = [
            self.r_z_k.conv, self.c_z_k.conv, self.r_x.conv, self.c_x.conv
        ]
        for ith in range(len(conv_list)):
            conv = conv_list[ith]
            torch.nn.init.normal_(conv.weight,
                                  std=conv_weight_std)  # conv_weight_std=0.01

    def set_device(self, dev):
        if not isinstance(dev, torch.device):
            dev = torch.device(dev)
        self.to(dev)
        if self.loss is not None:
            for loss in self.loss.values():
                loss.to(dev)
