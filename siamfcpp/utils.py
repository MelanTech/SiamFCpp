import collections

import cv2
import numpy as np
import torch

_MAX_RETRY = 50
_RETRY_NUM = 10


def cxywh2xyxy(box):
    box = np.array(box, dtype=np.float32)
    return np.concatenate([
        box[..., [0]] - (box[..., [2]] - 1) / 2,
        box[..., [1]] - (box[..., [3]] - 1) / 2,
        box[..., [0]] + (box[..., [2]] - 1) / 2,
        box[..., [1]] + (box[..., [3]] - 1) / 2,
    ], axis=-1)


def cxywh2xywh(box):
    box = np.array(box, dtype=np.float32)
    return np.concatenate([
        box[..., [0]] - (box[..., [2]] - 1) / 2,
        box[..., [1]] - (box[..., [3]] - 1) / 2,
        box[..., [2]], box[..., [3]]
    ], axis=-1)


def xyxy2cxywh(bbox):
    bbox = np.array(bbox, dtype=np.float32)
    return np.concatenate([
        (bbox[..., [0]] + bbox[..., [2]]) / 2,
        (bbox[..., [1]] + bbox[..., [3]]) / 2,
        bbox[..., [2]] - bbox[..., [0]] + 1,
        bbox[..., [3]] - bbox[..., [1]] + 1,
    ], axis=-1)


def xywh2cxywh(rect):
    rect = np.array(rect, dtype=np.float32)
    return np.concatenate([
        rect[..., [0]] + (rect[..., [2]] - 1) / 2,
        rect[..., [1]] + (rect[..., [3]] - 1) / 2,
        rect[..., [2]],
        rect[..., [3]]
    ], axis=-1)


def xywh2xyxy(rect):
    rect = np.array(rect, dtype=np.float32)
    return np.concatenate([
        rect[..., [0]], rect[..., [1]],
        rect[..., [2]] + rect[..., [0]] - 1,
        rect[..., [3]] + rect[..., [1]] - 1
    ], axis=-1)


def xyxy2xywh(bbox):
    bbox = np.array(bbox, dtype=np.float32)
    return np.concatenate([
        bbox[..., [0]], bbox[..., [1]],
        bbox[..., [2]] - bbox[..., [0]] + 1,
        bbox[..., [3]] - bbox[..., [1]] + 1
    ], axis=-1)


def get_subwindow_tracking(im,
                           pos,
                           model_sz,
                           original_sz,
                           avg_chans=(0, 0, 0),
                           mask=None):
    r"""
    Get subwindow via cv2.warpAffine

    Arguments
    ---------
    im: numpy.array
        original image, (H, W, C)
    pos: numpy.array
        subwindow position
    model_sz: int
        output size
    original_sz: int
        subwindow range on the original image
    avg_chans: tuple
        average values per channel
    mask: numpy.array
        mask, (H, W)


    Returns
    -------
    numpy.array
        image patch within _original_sz_ in _im_ and  resized to _model_sz_, padded by _avg_chans_
        (model_sz, model_sz, 3)
    """
    crop_cxywh = np.concatenate(
        [np.array(pos), np.array((original_sz, original_sz))], axis=-1)
    crop_xyxy = cxywh2xyxy(crop_cxywh)
    # warpAffine transform matrix
    M_13 = crop_xyxy[0]
    M_23 = crop_xyxy[1]
    M_11 = (crop_xyxy[2] - M_13) / (model_sz - 1)
    M_22 = (crop_xyxy[3] - M_23) / (model_sz - 1)
    mat2x3 = np.array([
        M_11,
        0,
        M_13,
        0,
        M_22,
        M_23,
    ]).reshape(2, 3)
    im_patch = cv2.warpAffine(im,
                              mat2x3, (model_sz, model_sz),
                              flags=(cv2.INTER_LINEAR | cv2.WARP_INVERSE_MAP),
                              borderMode=cv2.BORDER_CONSTANT,
                              borderValue=tuple(map(int, avg_chans)))
    if mask is not None:
        mask_patch = cv2.warpAffine(mask,
                                    mat2x3, (model_sz, model_sz),
                                    flags=(cv2.INTER_NEAREST
                                           | cv2.WARP_INVERSE_MAP))
        return im_patch, mask_patch
    return im_patch


def crop_track_pair(
        im_temp,
        bbox_temp,
        im_curr,
        bbox_curr,
        config=None,
        avg_chans=None,
        rng=np.random,
        DEBUG=False,
        mask_tmp=None,
        mask_curr=None,
):
    context_amount = config["context_amount"]
    z_size = config["z_size"]
    x_size = config["x_size"]
    max_scale = config["max_scale"]
    max_shift = config["max_shift"]
    max_scale_temp = config["max_scale_temp"]
    max_shift_temp = config["max_shift_temp"]

    if avg_chans is None:
        avg_chans = np.mean(im_temp, axis=(0, 1))
    box_temp = xyxy2cxywh(bbox_temp)
    box_curr = xyxy2cxywh(bbox_curr)

    # crop size, st for tamplate & sc for current
    wt, ht = box_temp[2:]
    wt_ = wt + context_amount * (wt + ht)
    ht_ = ht + context_amount * (wt + ht)
    st_ = np.sqrt(wt_ * ht_)

    wc, hc = box_curr[2:]
    wc_ = wc + context_amount * (wc + hc)
    hc_ = hc + context_amount * (wc + hc)
    sc_ = np.sqrt(wc_ * hc_)

    assert (st_ > 0) and (
            sc_ > 0), "Invalid box: box_temp %s and box_curr %s" % (str(bbox_temp),
                                                                    str(bbox_curr))

    scale_temp_ = z_size / st_
    scale_curr_ = z_size / sc_

    # loop to generate valid augmentation
    for i in range(_MAX_RETRY + 1):
        # random scale
        if i < _MAX_RETRY:
            s_max = 1 + max_scale
            s_min = 1 / s_max
            scale_rand = rng.uniform(s_min, s_max)
            s_max = 1 + max_scale_temp
            s_min = 1 / s_max
            scale_rand_temp = np.exp(rng.uniform(np.log(s_min), np.log(s_max)))
        else:
            scale_rand = scale_rand_temp = 1
            if DEBUG: print('not augmented')
        scale_curr = scale_curr_ / scale_rand
        scale_temp = scale_temp_ / scale_rand_temp
        s_curr = x_size / scale_curr
        s_temp = z_size / scale_temp

        # random shift
        if i < _MAX_RETRY:
            dx = rng.uniform(-max_shift, max_shift) * s_curr / 2
            dy = rng.uniform(-max_shift, max_shift) * s_curr / 2
            dx_temp = rng.uniform(-max_shift_temp, max_shift_temp) * s_temp / 2
            dy_temp = rng.uniform(-max_shift_temp, max_shift_temp) * s_temp / 2
        else:
            dx = dy = dx_temp = dy_temp = 0
            if DEBUG: print('not augmented')

        # calculate bbox for cropping
        box_crop_temp = np.concatenate([
            box_temp[:2] - np.array([dx_temp, dy_temp]),
            np.array([s_temp, s_temp])
        ])
        box_crop_curr = np.concatenate(
            [box_curr[:2] - np.array([dx, dy]),
             np.array([s_curr, s_curr])])

        # calculate new bbox
        box_z = np.array([(z_size - 1) / 2] * 2 + [0] * 2) + np.concatenate(
            [np.array([dx_temp, dy_temp]),
             np.array([wt, ht])]) * scale_temp
        box_x = np.array([(x_size - 1) / 2] * 2 + [0] * 2) + np.concatenate(
            [np.array([dx, dy]), np.array([wc, hc])]) * scale_curr
        bbox_z = cxywh2xyxy(box_z)
        bbox_x = cxywh2xyxy(box_x)

        # check validity of bbox
        if not (all([0 <= c <= z_size - 1 for c in bbox_z])
                and all([0 <= c <= x_size - 1 for c in bbox_x])):
            continue
        else:
            break

    # crop & resize via warpAffine
    mask_z = None
    mask_x = None
    if mask_tmp is not None:
        im_z, mask_z = get_subwindow_tracking(im_temp,
                                              box_crop_temp[:2],
                                              z_size,
                                              s_temp,
                                              avg_chans=avg_chans,
                                              mask=mask_tmp)
    else:
        im_z = get_subwindow_tracking(im_temp,
                                      box_crop_temp[:2],
                                      z_size,
                                      s_temp,
                                      avg_chans=avg_chans)

    if mask_curr is not None:
        im_x, mask_x = get_subwindow_tracking(im_curr,
                                              box_crop_curr[:2],
                                              x_size,
                                              s_curr,
                                              avg_chans=avg_chans,
                                              mask=mask_curr)
    else:
        im_x = get_subwindow_tracking(im_curr,
                                      box_crop_curr[:2],
                                      x_size,
                                      s_curr,
                                      avg_chans=avg_chans)

    return im_z, bbox_z, im_x, bbox_x, mask_z, mask_x


def make_densebox_target(gt_boxes: np.array, config):
    """
    Densebox的模型训练目标生成函数

    Arguments
    ---------
    gt_boxes : np.array
        具有类的真实边界框，shape=（N，5），order=（x0，y0，x1，y1，class）
    config: 目标生成配置
        Keys
        ----
        x_size : int
            search图像尺寸
        score_size : int
            得分特征图尺寸
        total_stride : int
            backbone的总步长
        score_offset : int
            得分图的边缘和搜索图像的边界之间的偏移

    Returns
    -------
    Tuple
        cls_res_final : np.array
            class
            shape=(N, 1)
        ctr_res_final : np.array
            shape=(N, 1)
        gt_boxes_res_final : np.array
            shape=(N, 4)
        # previous format
        # shape=(N, 6), order=(class, center-ness, left_offset, top_offset, right_offset, bottom_offset)
    """
    x_size = config["x_size"]
    score_size = config["score_size"]
    total_stride = config["total_stride"]
    score_offset = config["score_offset"]
    eps = 1e-5

    raw_height, raw_width = x_size, x_size  # 原始图像高宽

    # 判断gt_boxes是否包含置信度信息，如果不包含，则将置信度全部设置为1.0
    if gt_boxes.shape[1] == 4:
        gt_boxes = np.concatenate([gt_boxes, np.ones((gt_boxes.shape[0], 1))], axis=1)  # boxes_cnt x 5

    # 添加一个全零的box，防止box数为0，使后面不能正常计算面积
    gt_boxes = np.concatenate([np.zeros((1, 5)), gt_boxes])  # boxes_cnt x 5
    # 计算所有边界框面积
    gt_boxes_area = (np.abs((gt_boxes[:, 2] - gt_boxes[:, 0]) * (gt_boxes[:, 3] - gt_boxes[:, 1])))
    gt_boxes = gt_boxes[np.argsort(gt_boxes_area)]  # 按面积大小排序
    boxes_cnt = len(gt_boxes)  # 获取边界框数量

    # 创建一个二维的网格，其中每个点(x,y)表示feature map上的一个像素位置
    shift_x = np.arange(0, raw_width).reshape(-1, 1)
    shift_y = np.arange(0, raw_height).reshape(-1, 1)
    shift_x, shift_y = np.meshgrid(shift_x, shift_y)

    # 这里计算了每个像素点与所有box之间的四个坐标偏移量(off_l, off_t, off_r, off_b)
    # off_l表示当前像素点到box左边界的偏移量
    off_l = (shift_x[:, :, np.newaxis, np.newaxis] - gt_boxes[np.newaxis, np.newaxis, :, 0, np.newaxis])
    # off_t表示当前像素点到box上边界的偏移量
    off_t = (shift_y[:, :, np.newaxis, np.newaxis] - gt_boxes[np.newaxis, np.newaxis, :, 1, np.newaxis])
    # off_r表示当前像素点到box右边界的偏移量
    off_r = -(shift_x[:, :, np.newaxis, np.newaxis] - gt_boxes[np.newaxis, np.newaxis, :, 2, np.newaxis])
    # off_b表示当前像素点到box下边界的偏移量。
    off_b = -(shift_y[:, :, np.newaxis, np.newaxis] - gt_boxes[np.newaxis, np.newaxis, :, 3, np.newaxis])

    # 这里计算了每个像素点与对应box之间的中心度，即当前像素点是否位于该box的中心位置
    # 计算出相邻两个边界之间的最小距离并求其乘积，然后除以相邻两个边界之间的最大距离，得到中心度
    center = ((np.minimum(off_l, off_r) * np.minimum(off_t, off_b)) /
              (np.maximum(off_l, off_r) * np.maximum(off_t, off_b) + eps))
    center = np.squeeze(np.sqrt(np.abs(center)))

    # 由于上面添加了一个全零box，这里把它的中心度置为0，即不考虑它的中心度
    center[:, :, 0] = 0

    # 将四个偏移量拼接在一起，得到一个shape为(h, w, boxes_cnt*4)的数组
    offset = np.concatenate([off_l, off_t, off_r, off_b], axis=3)  # h * w * boxes_cnt * 4
    cls = gt_boxes[:, 4]

    cls_res_list = []
    ctr_res_list = []
    gt_boxes_res_list = []

    # score图尺寸
    fm_height, fm_width = score_size, score_size

    fm_size_list = []
    fm_strides = [total_stride]
    fm_offsets = [score_offset]

    # 通过循环计算出一系列不同尺度的feature map大小
    for fm_i in range(len(fm_strides)):
        fm_size_list.append([fm_height, fm_width])
        fm_height = int(np.ceil(fm_height / 2))
        fm_width = int(np.ceil(fm_width / 2))

    fm_size_list = fm_size_list[::-1]
    for fm_i, (stride, fm_offset) in enumerate(zip(fm_strides, fm_offsets)):
        fm_height = fm_size_list[fm_i][0]
        fm_width = fm_size_list[fm_i][1]

        shift_x = np.arange(0, fm_width)
        shift_y = np.arange(0, fm_height)
        shift_x, shift_y = np.meshgrid(shift_x, shift_y)

        # 将网格矩阵中的所有坐标点（二维）转换为一个数组（一维），其中每个元素有两个值（x和y）
        # 然后将数组转置，以便每个元素表示一个点。
        xy = np.vstack((shift_y.ravel(), shift_x.ravel())).transpose()  # (h*w) x 2

        # 计算当前特征图上每个点的偏移量
        off_xy = offset[fm_offset + xy[:, 0] * stride, fm_offset + xy[:, 1] * stride]  # will reduce dim by 1

        # 初始化一个三维数组，用于存储标记哪些偏移量处于GT框内
        off_valid = np.zeros((fm_height, fm_width, boxes_cnt))

        # 对于每个预测的偏移量，判断其是否在GT框内
        is_in_boxes = (off_xy > 0).all(axis=2)

        off_valid[xy[:, 0], xy[:, 1], :] = is_in_boxes
        off_valid[:, :, 0] = 0  # h x w x boxes_cnt

        # 确定每个预测点最接近哪个GT框
        hit_gt_ind = np.argmax(off_valid, axis=2)  # h x w

        # 将每个预测点最接近的GT框的坐标信息写入相应的数组元素中
        gt_boxes_res = np.zeros((fm_height, fm_width, 4))
        gt_boxes_res[xy[:, 0], xy[:, 1]] = gt_boxes[hit_gt_ind[xy[:, 0], xy[:, 1]], :4]
        gt_boxes_res_list.append(gt_boxes_res.reshape(-1, 4))

        # cls
        cls_res = np.zeros((fm_height, fm_width))
        cls_res[xy[:, 0], xy[:, 1]] = cls[hit_gt_ind[xy[:, 0], xy[:, 1]]]
        cls_res_list.append(cls_res.reshape(-1))

        # center
        center_res = np.zeros((fm_height, fm_width))
        center_res[xy[:, 0], xy[:, 1]] = center[fm_offset +
                                                xy[:, 0] * stride, fm_offset +
                                                xy[:, 1] * stride,
                                                hit_gt_ind[xy[:, 0], xy[:, 1]]]
        ctr_res_list.append(center_res.reshape(-1))

    cls_res_final = np.concatenate(cls_res_list,
                                   axis=0)[:, np.newaxis].astype(np.float32)
    ctr_res_final = np.concatenate(ctr_res_list,
                                   axis=0)[:, np.newaxis].astype(np.float32)
    gt_boxes_res_final = np.concatenate(gt_boxes_res_list,
                                        axis=0).astype(np.float32)

    return cls_res_final, ctr_res_final, gt_boxes_res_final


def convert_numpy_to_tensor(raw_data):
    r"""
    convert numpy array dict or list to torch.Tensor
    """
    elem_type = type(raw_data)
    if (elem_type.__module__ == "numpy" and elem_type.__name__ != "str_"
            and elem_type.__name__ != "string_"):
        return torch.from_numpy(raw_data).float()
    elif isinstance(raw_data, collections.abc.Mapping):
        data = {key: convert_numpy_to_tensor(raw_data[key]) for key in raw_data}
        if 'image' in data:
            data['image'] = data['image'].permute(2, 0, 1)
        return data
    elif isinstance(raw_data, collections.abc.Sequence):
        return [convert_numpy_to_tensor(data) for data in raw_data]
    else:
        return raw_data


def load_image(img_file: str) -> np.array:
    """Image loader used by data module (e.g. image sampler)

    Parameters
    ----------
    img_file: str
        path to image file
    Returns
    -------
    np.array
        loaded image

    Raises
    ------
    FileExistsError
        invalid image file
    RuntimeError
        unloadable image file
    """
    # read with OpenCV
    img = cv2.imread(img_file, cv2.IMREAD_COLOR)
    if img is None:
        # retrying
        for ith in range(_RETRY_NUM):
            img = cv2.imread(img_file, cv2.IMREAD_COLOR)
            if img is not None:
                break
    # read with PIL
    # img = Image.open(img_file)
    # img = np.array(img)
    # img = img[:, :, [2, 1, 0]]  # RGB -> BGR

    return img


def filter_unreasonable_training_boxes(im: np.array, bbox, config) -> bool:
    r"""
    过滤过小、过大的对象和具有极端比率的图像
    Arguments
    ---------
    im: np.array
        image, formate=(H, W, C)
    bbox: np.array or indexable object
        bounding box annotation in (x, y, w, h) format
    config:
    """
    eps = 1e-6
    im_area = im.shape[0] * im.shape[1]
    _, _, w, h = bbox
    bbox_area = w * h
    bbox_area_rate = bbox_area / im_area
    bbox_ratio = h / (w + eps)

    # 有效条件
    conds = [(config["min_area_rate"] < bbox_area_rate,
              bbox_area_rate < config["max_area_rate"]),
             max(bbox_ratio, 1.0 / max(bbox_ratio, eps)) < config["max_ratio"]]

    # 如果不是所有条件都满足，则过滤样本
    filter_flag = not all(conds)

    return filter_flag


def move_data_to_device(data_dict, dev):
    for k in data_dict:
        data_dict[k] = data_dict[k].to(dev)

    return data_dict
